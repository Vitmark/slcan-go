package slcan

import (
	"testing"
)

func compareSlcanToExpected(out []byte, expected string, t *testing.T) {
	var outStr = string(out[:])
	if outStr != expected {
		t.Fatalf("%s does not match %s", outStr, expected)
	}
}

func TestFrameToSlcan(t *testing.T) {
	var inputFrame = CANFrame{
		ID:   0x0AF,
		DLC:  2,
		RTR:  false,
		IDE:  false,
		Data: CANData{0x2, 0x30, 0x57},
	}
	var expectedSlcan = "t0AF20230\r"
	out, err := FrameToSlcan(inputFrame)
	if err != nil {
		t.Fatal("Failed to convert frame to slcan")
	}
	compareSlcanToExpected(out, expectedSlcan, t)
}

func TestFrameToSlcanExtended(t *testing.T) {
	var inputFrame = CANFrame{
		ID:   0x15AF,
		DLC:  8,
		RTR:  false,
		IDE:  true,
		Data: CANData{0x2, 0x30, 0x57},
	}
	var expectedSlcan = "T000015AF80230570000000000\r"
	out, err := FrameToSlcan(inputFrame)
	if err != nil {
		t.Fatal("Failed to convert frame to slcan")
	}
	compareSlcanToExpected(out, expectedSlcan, t)
}

func TestFrameToSlcanRemote(t *testing.T) {
	var inputFrame = CANFrame{
		ID:  0x058,
		DLC: 8,
		RTR: true,
		IDE: false,
	}
	var expectedSlcan = "r0588\r"
	out, err := FrameToSlcan(inputFrame)
	if err != nil {
		t.Fatal("Failed to convert frame to slcan")
	}
	compareSlcanToExpected(out, expectedSlcan, t)
}

func TestFrameToSlcanRemoteExtended(t *testing.T) {
	var inputFrame = CANFrame{
		ID:  0x15AF,
		DLC: 8,
		RTR: true,
		IDE: true,
	}
	var expectedSlcan = "R000015AF8\r"
	out, err := FrameToSlcan(inputFrame)
	if err != nil {
		t.Fatal("Failed to convert frame to slcan")
	}
	compareSlcanToExpected(out, expectedSlcan, t)
}

func TestFrameToSlcanInvalid(t *testing.T) {
	var inputFrame0 = CANFrame{
		ID:   0x15AF,
		DLC:  9,
		RTR:  false,
		IDE:  true,
		Data: CANData{0x2, 0x30, 0x57},
	}
	_, err := FrameToSlcan(inputFrame0)
	if err == nil {
		t.Fatal("Invalid frame did not return error")
	}
	var inputFrame1 = CANFrame{
		ID:   ExtendedMask + 1,
		DLC:  5,
		RTR:  false,
		IDE:  true,
		Data: CANData{0x2, 0x30, 0x57},
	}
	_, err = FrameToSlcan(inputFrame1)
	if err == nil {
		t.Fatal("Invalid frame did not return error")
	}
}

func TestSlcanToFrame(t *testing.T) {
	var expectedFrame = CANFrame{
		ID:   0x0AF,
		DLC:  2,
		RTR:  false,
		IDE:  false,
		Data: CANData{0x2, 0x30},
	}
	var inputSlcan = "t0AF20230\r"
	out, err := SlcanToFrame([]byte(inputSlcan))
	if err != nil {
		t.Fatalf("Failed to convert slcan to frame: %v", err)
	}
	if expectedFrame != *out {
		t.Fatalf("Parsed frame %#v does not match expected %#v", *out, expectedFrame)
	}

}

func TestSlcanToFrameExtended(t *testing.T) {
	var expectedFrame = CANFrame{
		ID:   0x15AF,
		DLC:  8,
		RTR:  false,
		IDE:  true,
		Data: CANData{0x2, 0x30, 0x57, 0x00, 0x00, 0x00, 0x00, 0x01},
	}
	var inputSlcan = "T000015AF80230570000000001\r"
	out, err := SlcanToFrame([]byte(inputSlcan))
	if err != nil {
		t.Fatalf("Failed to convert slcan to frame: %v", err)
	}
	if expectedFrame != *out {
		t.Fatalf("Parsed frame %#v does not match expected %#v", *out, expectedFrame)
	}

}

func TestSlcanToFrameRemote(t *testing.T) {
	var expectedFrame = CANFrame{
		ID:  0x058,
		DLC: 8,
		RTR: true,
		IDE: false,
	}
	var inputSlcan = "r0588\r"
	out, err := SlcanToFrame([]byte(inputSlcan))
	if err != nil {
		t.Fatalf("Failed to convert slcan to frame: %v", err)
	}
	if expectedFrame != *out {
		t.Fatalf("Parsed frame %#v does not match expected %#v", *out, expectedFrame)
	}
}

func TestSlcanToFrameRemoteExtended(t *testing.T) {
	var expectedFrame = CANFrame{
		ID:  0x15AF,
		DLC: 3,
		RTR: true,
		IDE: true,
	}
	var inputSlcan = "R000015AF3\r"
	out, err := SlcanToFrame([]byte(inputSlcan))
	if err != nil {
		t.Fatalf("Failed to convert slcan to frame: %v", err)
	}
	if expectedFrame != *out {
		t.Fatalf("Parsed frame %#v does not match expected %#v", *out, expectedFrame)
	}

}

func TestSlcanToFrameInvalid(t *testing.T) {
	var inputSlcan = "R000015AF9\r"
	_, err := SlcanToFrame([]byte(inputSlcan))
	if err == nil {
		t.Fatalf("Invalid input should end in error: %s", inputSlcan)
	}
}
