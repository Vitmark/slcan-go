package slcan

import (
	"fmt"
)

type CANData [8]byte

const StandardMask uint32 = (1 << 11) - 1
const ExtendedMask uint32 = (1 << 29) - 1

type CANFrame struct {
	ID   uint32
	RTR  bool
	IDE  bool
	DLC  uint8
	Data CANData
}

func (frame *CANFrame) Validate() error {
	if frame.IDE && frame.ID > ExtendedMask {
		return fmt.Errorf("Extended id too hight")
	} else if !frame.IDE && frame.ID > StandardMask {
		return fmt.Errorf("Standard id too hight")
	} else if frame.DLC > 8 {
		return fmt.Errorf("DLC higher than 8")
	}
	return nil
}

type Bitrate string

const (
	B10k  Bitrate = "S0\r"
	B20k  Bitrate = "S1\r"
	B50k  Bitrate = "S2\r"
	B100k Bitrate = "S3\r"
	B125k Bitrate = "S4\r"
	B250k Bitrate = "S5\r"
	B500k Bitrate = "S6\r"
	B750k Bitrate = "S7\r"
	B1M   Bitrate = "S8\r"
	B83k3 Bitrate = "S9\r"
)

type DataBitrate string

const (
	B2M DataBitrate = "Y2\r"
	B5M DataBitrate = "Y5\r"
)

type Command string

const (
	OpenChannel           Command = "O\r"
	CloseChannel          Command = "C\r"
	GetVersion            Command = "V\r"
	GetErrors             Command = "E\r"
	NormalMode            Command = "M0\r"
	SilentMode            Command = "M1\r"
	DisableRetransmission Command = "A0\r"
	EnableRetransmission  Command = "A1\r"
)

type FrameCommand byte

const (
	Data           FrameCommand = 't'
	DataExtended   FrameCommand = 'T'
	Remote         FrameCommand = 'r'
	RemoteExtended FrameCommand = 'R'
	FD             FrameCommand = 'd'
	FDExtended     FrameCommand = 'D'
	FDBRS          FrameCommand = 'b'
	FDBRSExtended  FrameCommand = 'B'
)

var standardFrameCmds = [...]FrameCommand{Data, DataExtended, Remote, RemoteExtended}
var fdFrameCmds = [...]FrameCommand{FD, FDExtended, FDBRS, FDBRSExtended}

func serializeFrameCommand(frame CANFrame) FrameCommand {
	if frame.IDE && frame.RTR {
		return RemoteExtended
	} else if frame.IDE && !frame.RTR {
		return DataExtended
	} else if !frame.IDE && frame.RTR {
		return Remote
	} else {
		return Data
	}
}

func parseFrameCommand(b byte) (FrameCommand, error) {
	for _, cmd := range standardFrameCmds {
		if b == byte(cmd) {
			return cmd, nil
		}
	}
	return 0, fmt.Errorf("Failed to parse FrameCommand")
}

func FrameToSlcan(frame CANFrame) ([]byte, error) {
	err := frame.Validate()
	if err != nil {
		return nil, err
	}

	id := frame.ID
	len := frame.DLC
	frameCmd := serializeFrameCommand(frame)
	var out []byte
	if frame.IDE {
		out = fmt.Appendf(out, "%c%08X%1d", frameCmd, id, len)
	} else {
		out = fmt.Appendf(out, "%c%03X%1d", frameCmd, id, len)
	}
	if !frame.RTR {
		for i := range frame.DLC {
			out = fmt.Appendf(out, "%02X", frame.Data[i])
		}
	}
	out = fmt.Append(out, "\r")

	return out, nil
}

func SlcanToFrame(slcan []byte) (*CANFrame, error) {
	var err error
	var startOffset = 0
	var inputLength = len(slcan)
	fcmd, err := parseFrameCommand(slcan[0])
	if err != nil {
		return nil, err
	}
	startOffset += 1

	var outFrame = CANFrame{}
	switch fcmd {
	case Data:
		outFrame.IDE = false
		outFrame.RTR = false
	case DataExtended:
		outFrame.IDE = true
		outFrame.RTR = false
	case Remote:
		outFrame.IDE = false
		outFrame.RTR = true
	case RemoteExtended:
		outFrame.IDE = true
		outFrame.RTR = true
	}

	var scanString = string(slcan[startOffset:])
	if outFrame.IDE {
		if (inputLength - startOffset) < 9 {
			return nil, fmt.Errorf("Input string is not long enough")
		}
		_, err = fmt.Sscanf(scanString, "%08X%1d", &outFrame.ID, &outFrame.DLC)
		startOffset += 9
	} else {
		if (inputLength - startOffset) < 4 {
			return nil, fmt.Errorf("Input string is not long enough")
		}
		_, err = fmt.Sscanf(scanString, "%03X%1d", &outFrame.ID, &outFrame.DLC)
		startOffset += 4
	}
	if err != nil {
		return nil, err
	}

	if outFrame.DLC > 8 {
		return nil, fmt.Errorf("Found DLC > 8")
	}

	if !outFrame.RTR {
		var dlc = int(outFrame.DLC)
		if (inputLength - startOffset) < (2 * dlc) {
			return nil, fmt.Errorf("Input string is not long enough")
		}
		for i := range dlc {
			scanString = string(slcan[startOffset : startOffset+2])
			_, err = fmt.Sscanf(scanString, "%02X", &outFrame.Data[i])
			if err != nil {
				return nil, err
			}
			startOffset += 2
		}
	}

	return &outFrame, nil
}
